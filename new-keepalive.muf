( new-keepalive.muf - Improved Keepalive           - by Talako@SPR     )
( Installation:                                                        )
( 1. Set program WIZARD, AUTOSTART, and LINK_OK.                       )
( 2. Create action named @keepalive on #0 and link it to this program. )
( 3. Set your $defs below.                                             )
( 4. This program will autostart the moment you compile it.            )

( BEGIN CONFIG )
$def dataobj prog                ( Object where data is stored         )
$def datadir "~keepalive"        ( Data storage propdir                )
$def cycletime 15                ( Program polls every x seconds       )
$def mincycles 4                 ( Can't set maxidle lower than        )
                                 (   cycletime * mincycles             )
$def defmaxidle 300              ( Max idle time if not set            )
$def defmsg "Ping!"              ( Ping message if not set             )
( END CONFIG )

$def arr_tell { me @ }array array_notify

: update-timestamp
  datadir "/lastnoise" strcat systime setprop
;

: cycle-init
  dataobj datadir "/pid" strcat getpropval pid = not if exit then
  dataobj datadir "/plist" strcat array_get_reflist foreach
    nip dup dup datadir "/lastnoise" strcat getpropval swap
    datadir "/maxidle" strcat getpropval +
    systime cycletime + <= if
      dup dup datadir "/message" strcat getpropstr systime timefmt notify
      update-timestamp
    then
  repeat
  cycletime prog "KA-Timer" queue dataobj swap datadir "/pid" strcat swap setprop
;

: do-kickstart 
  dataobj datadir "/pid" strcat pid setprop
  cycle-init
;

: do-enable
  dataobj datadir "/plist" strcat me @ reflist_add
  me @ datadir "/maxidle" strcat defmaxidle setprop
  me @ datadir "/message" strcat defmsg setprop
  me @ "~listen/keepalive" prog setprop
  me @ "Keepalive enabled with default max idle of " defmaxidle intostr strcat " seconds." strcat notify
;

: do-disable
  dataobj datadir "/plist" strcat me @ reflist_del
  me @ "~listen/keepalive" remove_prop
  me @ "Keepalive disabled." notify
;

: do-set-message
  " " split nip dup me @ swap datadir "/message" strcat swap setprop
  dup me @ swap "Keepalive message set to: " swap strcat notify
  me @ swap "Example keepalive message: " swap strcat systime timefmt notify
;

: do-set-maxidle
  " " split nip atoi dup if
    dup cycletime mincycles * < if
      me @ "You can't set your maximum idle time to less than " cycletime mincycles * intostr strcat " seconds." strcat notify
    else
      dup me @ swap datadir "/maxidle" strcat swap setprop
      me @ swap "Max idle time set to " swap intostr strcat " seconds." strcat notify
    then
  else
    me @ "Please provide a valid number of seconds for your maximum idle time." notify
  then
;

: do-help
  { "=== new-keepalive.muf =================================== Talako@SPR ==="
    "This keepalive program will only ping you if you haven't seen any other"
    "activity within your configured max idle period."
    " "
    "  " command @ " #on                - Turn on keepalives" strcat strcat
    "  " command @ " #off               - Turn off keepalives" strcat strcat
    " "
    "  " command @ " #maxidle <seconds> - Set max idle time in seconds" strcat strcat
    "  " command @ strlen " " * strcat "                      Minimum: " strcat cycletime mincycles * intostr strcat " seconds" strcat
    " "
    "  " command @ " #message <message> - Set your keepalive message" strcat strcat
    "  " command @ strlen " " * strcat "                      Supports TIMEFMT strings" strcat
    " "
    "  " command @ " #help              - This screen" strcat strcat
    me @ "WIZARD" flag? if
      " "
      "  " command @ " #kickstart         - Launch the polling routine (wizard only)" strcat strcat
    then
    "========================================================================"
  }array arr_tell
;

: main
  command @ "(_Listen)" strcmp not if
    me @ update-timestamp
    exit
  then

  dup "Startup" strcmp not if
    depth popn do-kickstart
    exit
  then

  dup "KA-Timer" strcmp not if
    depth popn cycle-init
    exit
  then

  "me" pmatch me !

  dup "#kickstart" smatch if ( launch poller routine )
    me @ "WIZARD" flag? if
      depth popn background cycle-init
    else
      me @ "Only wizards can use that function." notify
    then
    exit
  then

  dup "#on" smatch if
    do-enable
    exit
  then

  dup "#off" smatch if
    do-disable
    exit
  then

  dup "#maxidle*" smatch if
    do-set-maxidle
    exit
  then

  dup "#message*" smatch if
    do-set-message
    exit
  then

  do-help
;