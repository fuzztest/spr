( auto-char-gen.muf - Automated Character Creation - by Talako@SPR )
( Installation:                                                    )
( 1. Set program W.                                                )
( 2. Create action named @charreq;@charadmin on #0 and link it.    )
( 3. Set your $defs below.                                         )
( 4. Set up some kind of monitor on the fuzzball user log to       )
(    handle character requests.                                    )

$def dataobj prog                ( Object where data is stored )
$def datadir "@requests"         ( Data storage propdir        )
$def expdur 86400                ( Seconds until token expires )  
$def emailprop "@/email"
$def wemailprop "@/wemail"
$def startnameprop "@/startname"
$def createdbyprop "@/createdby"
$def emailpattern "[A-Za-z0-9\\.\\_\\-\\+]+@[A-Za-z0-9\\-]+(?:\\.[A-Za-z0-9\\-]+)+"
$def arr_tell { me @ }array swap array_notify

lvar email
lvar token

: gen-salt ( -- str:salt )
  { 1 8 1 for pop "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789" random 62 % 1 + 1 midstr repeat }join
;

: gen-token ( -- )
  gen-salt email @ strcat systime intostr strcat md5hash token !
;

: create-request ( -- )
  gen-token
  dataobj datadir "/" token @ strcat strcat
  {
    "email" email @
    "exptime" systime expdur +
  }dict array_put_propvals
  email @ ":" token @ strcat strcat userlog
;

: check-existing-requests ( bool )
  dataobj datadir array_get_propdirs foreach
    nip dataobj swap datadir "/" strcat swap "/email" strcat strcat getpropstr
    email @ stringcmp not if 1 break then
  repeat
  0
;

: do-guest-req
  { "You must provide a valid email address in order to register a character."
    "This address will only be used for registration and password resets."
    "Enter email address:"
  }array arr_tell

  read dup emailpattern 1 regexp if

    email !

    { "You have entered: " email @ strcat
      "Please confirm that this address is correct (Y/N): "
    }array arr_tell

    read "y*" smatch if
    
      check-existing-requests if
        { "There is already a pending request for that email address."
          "Please check your email or notify a wizard for assistance."
        }array arr_tell
      else
        me @ "Creating request..." notify
        create-request
      then
    
    else

      me @ "Aborting. Please run @charreq to try again." notify

    then

  else

    { "That does not appear to be a valid email address."
      "Please try your request again or notify a wizard for assistance."
    }array arr_tell

  then  
;

: reap-expired-requests
  dataobj datadir array_get_propdirs foreach
    nip dup
    dataobj swap datadir "/" strcat swap "/exptime" strcat strcat getpropval
    systime < if
      dataobj swap datadir "/" strcat swap strcat remove_prop
    then
  repeat
;

: main
  reap-expired-requests

  "me" pmatch me !

  command @ "@charreq" smatch if

    me @ "GUEST" flag? if
      do-guest-req
    else
      do-alt-req
    then

  else

    command @ "@charadmin" smatch if
      me @ "This command is not yet implemented." notify
    else
      me @ "That is not a valid command within this program." notify
    then

  then
;